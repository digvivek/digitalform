$(document).ready(function(){
	var imageName ='';
		/*$(".resize-range").on("input", function(){
			$(".resize-range-lable").html("Resize : "+this.value*10+"%");
		});*/
		$(".userimage").on("input", function(){
			
			var image = this.files[0];
			 imageName = this.files[0].name;
			var url = URL.createObjectURL(image);
			console.log(url);
			$(".preview-image").attr("src", url);
		});
		$(".resize-image-form").on("submit", function(e){
			e.preventDefault();
			console.log(new FormData(this));
			// console.log(new FormData(this));
			$.ajax({
				type : "post",
				url : "php_image/image_compress.php",
				data : new FormData(this),
				processData : false,
				contentType : false,
				cache : false,
				xhr : function(){
					var request = new XMLHttpRequest();
					request.upload.onprogress = function(e){
						$(".resize-image-form").addClass("d-none");
						$(".preview-imgae").addClass("d-none");
						$(".uploading-status-con").removeClass("d-none");
						var loaded = (e.loaded/1024/1024).toFixed(2);
						var total = (e.total/1024/1024).toFixed(2);
						var persentage = ((loaded*100)/total).toFixed(2);
						$(".progress-bar").css({width: persentage+"%"});
						$(".progress-bar").html(persentage+"%");
					}
					
					return request;
				},
				beforeSend : function(){
					// $(".compressing-status-con").removeClass("d-none");
				},
				success : function(r)
				{
					console.log(r);
					// $(".compressing-status-con").addClass("d-none");
					$(".uploading-status-con").addClass("d-none");
					// $(".compressing-status-con").addClass("d-none");
					$(".preview-imgae").removeClass("d-none");
					$(".resize-image-form").removeClass("d-none");
					$(".resize-image-form").trigger("reset");
					$(".resize-btn").html('RESIZE');
					
					$(".preview-imgae").attr("src", "upload/"+r);
	
					$(".resize-image-form").hide();
					$(".download-image-btn").removeClass("d-none");
					
					$(".download-image-btn").attr("href","upload/"+r);
					$(".download-image-btn").attr('download', r);
					$(".download-image-btn").click(function(){
						$(".resize-image-form").show();
						$(this).addClass("d-none");
						$(".preview-imgae").attr("src", "");
					});
				},
				error : function(error)
				{
					console.log(error);
				}
			});
		});
	});
