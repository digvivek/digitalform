<?php 

function compressImage($source, $destination, $quality) { 
    // Get image info 
    $imgInfo = getimagesize($source); 
    $mime = $imgInfo['mime']; 
     
    // Create a new image from file 
    switch($mime){ 
        case 'image/jpeg': 
            $image = imagecreatefromjpeg($source); 
            break; 
        case 'image/png': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/gif': 
            $image = imagecreatefromgif($source); 
            break; 
        default: 
            $image = imagecreatefromjpeg($source); 
    } 
     
    // Save image 
    imagejpeg($image, $destination, $quality); 
     
    // Return compressed image 
    return $destination; 
}
function convert_filesize($kbs, $decimals = 2) { 
    //$size = array('B','KB','MB','GB','TB','PB','EB','ZB','YB'); 
    //$size = array('KB'); 
    //$factor = floor((strlen($bytes) - 1) / 3); 

    //return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor]; 
    return floor($kbs/1024);
}
$status=0;
$uploadPath = "user_compress_upload_image/";
$fileName = basename($_FILES["userimage"]["name"]); 
$imageUploadPath = $uploadPath . $fileName; 
$fileType = pathinfo($imageUploadPath, PATHINFO_EXTENSION);

$resize_range=$_POST['resize-range'];
$allowTypes = array('jpg','png','jpeg','gif'); 
        if(in_array($fileType, $allowTypes)){ 
            // Image temp source and size 
            $imageTemp = $_FILES["userimage"]["tmp_name"]; 
            $imageSize = convert_filesize($resize_range); 
             
            // Compress size and upload image 
            $compressedImage = compressImage($imageTemp, $imageUploadPath, 75); 
             
            if($compressedImage){ 
                $compressedImageSize = filesize($compressedImage); 
                $compressedImageSize = convert_filesize($compressedImageSize); 
                 
                $status = 1; 
                
                
            }else{ 
                $statusMsg = "Image compress failed!"; 
            } 
        }else{ 
            $statusMsg = 'Sorry, only JPG, JPEG, PNG, & GIF files are allowed to upload.'; 
        } 

echo $statusMsg;  ?>

<?php if(!empty($compressedImage)){ ?>
    <p><b>Original Image Size:</b> <?php echo $imageSize; ?></p>
    <p><b>Compressed Image Size:</b> <?php echo $compressedImageSize; ?></p>
    <img src="<?php echo $compressedImage; ?>"/>
<?php } ?>
?>