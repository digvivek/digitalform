<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark" aria-label="Main navigation">
  <div class="container-fluid">
    <a class="navbar-brand" href="dashboard.php"><?php echo SITE_NAME; ?></a>
    <button class="navbar-toggler p-0 border-0" type="button" id="navbarSideCollapse" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
       
        
      </ul>
      <div class="d-flex">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">
            Welcome to <?php echo SITE_NAME; ?>  CSC Center : <?php echo strtoupper($_SESSION["cscname"]); ?>
          </a>
        </li>
        <!--<li class="nav-item">
          <a class="nav-link" href="#">Notifications</a>
        </li>-->
        
        <li class="nav-item">
          <a class="nav-link active" href="#"><?php echo ucwords($_SESSION["uname"]); ?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="logout.php">Logout</a>
        </li>
        <!--<li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-expanded="false"><?php echo ucwords($_SESSION["uname"]); ?></a>-->
         
        </li>
      </ul>
      </div>
    </div>
  </div>
</nav>
