<?php 
include('auths.php');
include('includes/comman.php');
$json_cast_list =file_get_contents("includes/cast-subcast-list.json");
$json_cast = json_decode($json_cast_list, true);



 ?>
<html  data-bs-theme="auto">
  <head><script src="../assets/js/color-modes.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.112.5">
    <title><?php echo SITE_NAME; ?> | Dashboard</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/offcanvas-navbar.css" rel="stylesheet">
  </head>
   <body class="bg-light">
   <?php
        include('navbar.php');
    ?>



<main class="container">
 
  <div class="album py-2 bg-light">
  <div class="container">

  <form action="self_dec_prev.php" method="post">
   <div class="row">
    <div class="col">
      <label for="exampleFormControlInput1" class="form-label">प्रमाण पत्र बनवाने का कारण</label>
      <input type="text" class="form-control" placeholder="प्रमाण पत्र बनवाने का कारण" name="email">
    </div>
    <div class="col">
     <label for="exampleFormControlInput1" class="form-label ">आधार न.</label>
  <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="आवेदक का आधार नम्बर" required>
    </div>
  </div>

<div class="col mt-3">
<div class="d-flex">
  प्रमाण पत्र:-&ensp; &ensp;
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked >

  <label class="form-check-label" for="flexCheckChecked">
    आय प्रमाण पत्र
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
  <label class="form-check-label" for="flexCheckChecked">
    जाति प्रमाण पत्र
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
  <label class="form-check-label" for="flexCheckChecked">
    निवास प्रमाण पत्र
  </label>
</div>
</div>

  <div class="row">
    <div class="col">
  <div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 ">आवेदक का नाम </label>
  <input type="text" class="form-control" id="exampleFormControlInput1">
</div>
    </div>
  <div class="col d-flex mt-5">
 <div class="form-check">
  <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
  <label class="form-check-label" for="flexRadioDefault2">
    पुत्र
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
  <label class="form-check-label" for="flexRadioDefault2">
   पुत्री
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
  <label class="form-check-label" for="flexRadioDefault2">
   पति
  </label>
</div>
    </div>
  </div>



  <div class="row">
    <div class="col">
<div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 " >पिता/पति का नाम</label>
  <input type="text" class="form-control" id="exampleFormControlInput1" required>
</div>
    </div>
    <div class="col">
<div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 " >माता का नाम</label>
  <input type="text" class="form-control" id="exampleFormControlInput1" required>
</div>
    </div>
  </div>


  <div class="row">
    <div class="col">
  <div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 ">जन्म तिथि</label>
  <input type="date" class="form-control" id="exampleFormControlInput1" required>
</div>
    </div>
    <div class="col">
<div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 " >व्यवसाय</label>
  <input type="text" class="form-control" id="exampleFormControlInput1" required>
</div>
    </div>
  </div>









  <div class="row">
    <div class="col">
   <div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 " >जन्म. स्थान</label>
  <input type="text" class="form-control" id="exampleFormControlInput1" required>
</div>
    </div>
    <div class="col">
<div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 " >जाति</label>
 
 <select class="form-select" id="maincast" onchange="selectCast();" aria-label="Default select example">
    <option selected>--Select Cast--</option>
     <?php
       foreach ($json_cast as $key=>$value) {
        ?>
       <option value="<?php echo $key  ?>"><?php echo $key;  ?></option>
     <?php 
       }
      ?>        
  </select>
</div>
    </div>
     <div class="col">
<div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 " >उपजाति</label>
 
 <select class="form-select" id="subcast" aria-label="Default select example">
          
  </select>
</div>
    </div>
  </div>
      <div class="row">
    <div class="col">
 <div class="mb-3">
  <label for="exampleFormControlTextarea1" class="form-label">निवास</label>
  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
</div> 
    </div>
    <div class="col">
<div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 " >जिला</label>
  <input type="text" class="form-control" id="exampleFormControlInput1" required>
</div>
    </div>
  </div>

        <div class="row">
    <div class="col">
<div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 " >तहसील</label>
  <input type="text" class="form-control" id="exampleFormControlInput1" required>
</div> 
    </div>
    <div class="col">
<div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 " >थाना</label>
  <input type="text" class="form-control" id="exampleFormControlInput1" required>
</div>
    </div>

        <div class="row">
    <div class="col">
<div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 " >स्थान</label>
  <input type="text" class="form-control" id="exampleFormControlInput1" required>
</div> 
    </div>
    <div class="col">
<div class="mb-3">
<div class="mb-3">
 <label for="exampleFormControlInput1" class="form-label mt-3 " >मोबाईल न.</label>
  <input type="number" class="form-control" id="exampleFormControlInput1" required>
</div>
</div>
    </div>

  </div>
  
<div class="d-grid gap-2">
  <button class="btn btn-success" type="submit">Button</button><br> -->
</div>
</form>
</div>
</main>



<?php  include('footer.php'); ?>

<script type="text/javascript">
  function selectCast() {
      let maincast=document.getElementById('maincast').value;
      //alert(maincast);
      $.ajax({
      type: "POST",
      url: "get_subcast.php",
      data:'maincast='+maincast,
      success: function(data){
        $("#subcast").html(data);
      }
      });
  }
 
</script>

<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="js/bootstrap.bundle.min.js"></script>

    </body>
</html>
