<?php 

include('auths.php');
include('includes/comman.php');


 ?>
<html lang="en" data-bs-theme="auto">
  <head><script src="../assets/js/color-modes.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.112.5">
    <title><?php echo SITE_NAME; ?> | Dashboard</title>
    <link href="css/bootstrap.css" rel="stylesheet">

    

    
    <!-- Custom styles for this template -->
    <link href="css/offcanvas-navbar.css" rel="stylesheet">
  </head>
   <body class="bg-light">
   <?php
        include('navbar.php');
    ?>



<main class="container">
  <div class="d-flex align-items-center p-3 my-3 text-white bg-purple rounded shadow-sm">
   
    <div class="lh-1">
      <h1 class="h6 mb-0 text-white lh-1">Welcome To Digital Form </h1>
      <small>Powered by SSV</small>
    </div>
  </div>

  

<div class="border border-warning border-3 rounded d-flex p-2 bd-highlight">
  <form class="p-4 resize-image-form" enctype="multipart/form-data" action="php/image_compress.php" method="post">
                <div class="form-group">
                    <input type="file" name="userimage" accept=".jpg, .png, .jpeg" class="form-control userimage" required="required" multiple="multiple">
                </div>
                <div class="form-group mt-5">

                    <lable class="text-secondary my-2">Please enter size in Kb</lable>
                    <input type="number" required="" name="resize-range" class="form-control resize-range" placeholder="20 KB" value="200">
                    <p class="text-center"></p>
                </div>
                <div class="alert alert-success text-center">अब आप एक से अधिक इमेजइ एव हस्ताक्षर एक बार में RESIZE कर सकते है !</div>
               
                <div class="form-group mt-5 text-right text-center">
                  <button type="submit" class="btn btn-primary active">Resize Image</button>
                </div>
                <!-- <div class="preview-image"></div>
                <center><img  class="preview-image shadow-lg mb-2" style="width:480px;border: 2px dashed #fff;">
            </center> -->
           </form>


           <div class="border border-3 rounded img-thumbnail">
  
                <div class="preview-image"></div>
                <center><img  class="preview-image shadow-lg mb-2"  style="width:480px;border: 2px dashed #fff; height: auto;">
            </center>
           </form>
         
          
        

    
  
</main>



<?php  include('footer.php'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="js/imageFunction.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>

    </body>
</html>
