<?php
session_start();
if(!empty($_SESSION['cid'])){
  header('location:dashboard.php');
}
include('includes/comman.php');
include('includes/db_connect.php');
 $msg='';
if(isset($_POST['submit'])){
   $email=$_POST['email']; 
   $pass=md5($_POST['password']);
   
   $csql="Select * from users where uemail='$email'";
   $rr=mysqli_query($conn, $csql);
   $tcount=mysqli_num_rows($rr);
   if($tcount==0){
    $msg="Your Email  $email is not register with Us";
    
   }
   else {

   $csql="Select cid,cscname,uname,uemail from users where uemail='$email' and upass='$pass'";
    $rr=mysqli_query($conn, $csql);
    $tcount=mysqli_num_rows($rr);
    //$tcountRecode=$tcount['tcount'];
    if($tcount>0){
      $fetch=mysqli_fetch_array($rr);
      //print_r($fetch);  
      $cid=$fetch['cid']; $uname=$fetch['uname'];
      $cscname=$fetch['cscname']; $uemail=$fetch['uemail'];
      $_SESSION["cid"]=$cid; $_SESSION["uname"]=$uname;
      $_SESSION["cscname"]=$cscname; $_SESSION["uemail"]=$uemail;
      header('location:dashboard.php');
    }  
    else{
      $msg="Email or password not correct / Login Fail";
    }


  }

}

    ?> 
<!doctype html>
<html lang="en" data-bs-theme="auto">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.112.5">
    <title><?php echo SITE_NAME; ?> | Login</title>
    <link href="css/bootstrap.css" rel="stylesheet">

    

    
    <!-- Custom styles for this template -->
    <link href="css/offcanvas-navbar.css" rel="stylesheet">
    <style type="text/css">
      .msg{ color: red; } 
    </style>
  </head>
  <body class="bg-body-tertiary">
   <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark" aria-label="Main navigation">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><?php echo SITE_NAME; ?></a>
    <button class="navbar-toggler p-1 border-0" type="button" id="navbarSideCollapse" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
<div class="d-flex" role="search">
       <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item ">
          <a class="nav-link active" aria-current="page" href="index.php">Login</a>
          <!-- <button type="submit" name="submit">Login</button> -->
        </li>
        <li class="nav-item">
          <a class="nav-link " href="reg.php">Registration</a>
        </li>
      </ul>
      </div>
  
      
       
          
      
    </div>
  </div>
</nav>


<main class="container">
  

  <div class="my-3 p-3 bg-body rounded shadow-sm">
    
    <section class="vh-100">
  <div class="container-fluid h-custom">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-md-9 col-lg-6 col-xl-5">
        <img src="img/draw2.webp" class="img-fluid" alt="Sample image">
      </div>
      <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
          <div class="d-flex flex-row align-items-center justify-content-center justify-content-lg-start">
            <p class="lead fw-normal mb-0 me-3">Sign in</p>

            
          </div>
          <?php echo  "<p class='msg'>".$msg."</p>"; ?> 
          

          <!-- Email input -->
          <div class="form-outline mb-4">
            <label class="form-label" for="form3Example3">Email address</label>
            <input type="email" name="email" id="email" class="form-control form-control-lg"
              placeholder="Enter a valid email address" />
            
          </div>

          <!-- Password input -->
          <div class="form-outline mb-3">
            <label class="form-label" for="form3Example4">Password</label>
            <input type="password" name="password" id="password" class="form-control form-control-lg"
              placeholder="Enter password" />
            
          </div>

          <div class="d-flex justify-content-between align-items-center">
            <!-- Checkbox -->
            
            <a href="#!" class="text-body">Forgot password?</a>
          </div>

          <div class="text-center text-lg-start mt-4 pt-2">
            <button type="sumbit" name="submit" class="btn btn-primary btn-lg"
              style="padding-left: 2.5rem; padding-right: 2.5rem;">Login</button>
            <p class="small fw-bold mt-2 pt-1 mb-0">Don't have an account? <a href="reg.php"
                class="link-danger">Register</a></p>
          </div>

        </form>
      </div>
    </div>
  </div>
  
</section>
  </div>

  

</main>
<?php  include('footer.php'); ?>
<script src="js/bootstrap.bundle.min.js"></script>

    </body>
</html>
