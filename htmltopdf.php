<?php 
include('auths.php');
include('includes/comman.php');


 ?>
<html lang="en" data-bs-theme="auto">
  <head><script src="../assets/js/color-modes.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.112.5">
    <title><?php echo SITE_NAME; ?> | Dashboard</title>
    <link href="css/bootstrap.css" rel="stylesheet">

    

    
    <!-- Custom styles for this template -->
    <link href="css/offcanvas-navbar.css" rel="stylesheet">
  </head>
   <body class="bg-light">
   <?php
        include('navbar.php');
    ?>



<main class="container">
  <div class="d-flex align-items-center p-3 my-3 text-white bg-purple rounded shadow-sm">
   
    <div class="lh-1">
      <h1 class="h6 mb-0 text-white lh-1">Welcome To Digital Form </h1>
      <small>Powered by SSV</small>
    </div>
  </div>  
</main>
<div class=" d-flex align-items-center justify-content-center" >
  <div class="mt-4" style="border:0px solid red;">
  <h2>HTML to PDF Converter</h2>
    <button type="button" class="btn btn-danger text-center">Select HTML file</button>
        <img src="drive.jpg" class="rounded" alt="Cinque Terre" width="50" height="38">
    
            <h3>or drop PDF here</h3>
            <div class="mt-4 p-2 bg-primary text-white rounded">
  <p>Make HTML spreadsheets easy to read by converting them to PDF.</p>
</div>
</div>
</div>



<?php  include('footer.php'); ?>
<script src="js/bootstrap.bundle.min.js"></script>

    </body>
</html>
