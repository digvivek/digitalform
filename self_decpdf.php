<?php
 include('auths.php');
include('includes/comman.php');

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>page</title>
	<style type="text/css">
	body{
		margin:0;
		padding: 0;
		font: 12pt "Tahoma";
	}
	*{
		box-sizing: border-box;
		-moz-box-sizing:border-box;
	}
	.page{
		width: 21cm;
		min-height: 29.7cm;
		margin: 1cm auto;
		padding: 2cm;
		border: 1px solid black;
		border-radius: 5px;
		background-color: white;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
	}
	.subpage{
		padding: 1cm;
		border: 5px red solid;
		height: 256mm;
		width: mm;
	}
	@page{
		size: A4;
		margin: 0;
	}
	@media print{
		.page{
			margin: 0;
			border: initial;
			border-radius: initial;
			width: initial;
			min-height: initial;
			box-shadow: initial;
			background: initial;
			page-break-after: always;
		}
	}

.R_Adhar{
	
	display: flex;
	
}
.floatr{
	width: 40%;
	float: right;	
}
.floatl{
	width: 60%;
	float: left;
}

.heading,.c_type{
	text-align: center;
	text-decoration: 3px underline;
}
.t_bold{
	font-weight: bold;
}
.t_font{
	text-align: justify;
}
.m_t{
	margin-top: 10px;
}



	</style>
</head>
<body>
<div class="book">
	<div class="page">
		<div class="R_Adhar">
			<div class="t_font floatl">प्रमाण पत्र बनवाने का कारण<span></span></div>
			<div class="t_font floatr">आधार न.<span></span></div>
		</div>
		<h2 class="heading">स्वप्रमाणित घोषणा पत्र</h2>
     <div class="c_type t_bold"><span></span>हेतु</div><br>

<div class="t_font">
&emsp;&emsp;&emsp;मैं <span></span> <span></span> श्री <span></span> माता श्रीमती <span></span>
जन्म तिथि <span></span>  व्यवसाय <span></span> जन्म. स्थान <span></span> जाति <span></span>
उपजाति <span></span> निवास <span></span> थाना <span></span>  तहसील <span></span>
जिला <span></span> घोषणा करता <span></span> /करती <span></span> हूँ कि आवेदन पत्र में दिए गये विवरण तथ्य एवं मेरी जानकारी विश्वास में शुध्द

एवं सत्य है, मैं मिथ्या/विवरण तथ्यों को देने के परिणामों से भलीकांति अवगत हूँ ! यदि आवेदन पत्र में दिए गये कोई
विवरण/तथ्य मिथ्य पाए जाते है, तो मेरे विरुद्ध भा.द.वि. 960 कि धारा - 99 व 200 एवं प्रभावी किसी अन्य विधि
के अंतर्गत अभियोजन एवं दण्ड के लिए स्वयं उत्तर दायी होऊंगा <span></span>  होउंगी <span></span>।<br>
<div class="R_Adhar m_t" >
			<div class="t_font floatl">दिनांक<span></span></div>
			<div class="t_font floatr">मोबाईल न.<span></span></div>
		</div>
<div class="R_Adhar m_t">
			<div class="t_font floatl ">स्थान<span></span></div>
			<div class="t_font floatr">आवेदक के हस्ताक्षर</div>
		</div>
</div>

	</div>
</div>







</body>
</html>