<?php
//phpinfo();
include('auths.php');
include('includes/comman.php');


 ?>
<html lang="en" data-bs-theme="auto">
  <head><script src="../assets/js/color-modes.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.112.5">
    <title><?php echo SITE_NAME; ?> | Dashboard</title>
    <link href="css/bootstrap.css" rel="stylesheet">

    

    
    <!-- Custom styles for this template -->
    <link href="css/offcanvas-navbar.css" rel="stylesheet">
  </head>
   <body class="bg-light">
   <?php
        include('navbar.php');
    ?>



<main class="container">
  <div class="d-flex align-items-center p-3 my-3 text-white bg-purple rounded shadow-sm">
   
    <div class="lh-1">
      <h1 class="h6 mb-0 text-white lh-1">Welcome To Digital Form </h1>
      <small>Powered by SSV </small>
    </div>
  </div>

  <div class="album py-5 bg-light">
    <div class="container">

      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
        <div class="col">
          <div class="card shadow-sm">
            

            <div class="card-body">
              <p class="card-text"><a href="#" class="btn btn-primary ">Banking Services Form</a></p>
              <p><select class="form-select" aria-label="Default select example">
              <option selected>--Select Bank Service--</option>
              <option value="1">Account Close Form</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
              </select></p>
            
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card shadow-sm">
            

            <div class="card-body">
               <p class="card-text"><a href="#" class="btn btn-primary ">Income Cost & Domicile Certificate From </a></p>

               <p class="card-text"><a href="self_dec.php">Self Declartion Form</a></p>

              <div class="d-flex justify-content-between align-items-center">
              </div>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card shadow-sm">
            

            <div class="card-body">
              <p class="card-text"><a href="#" class="btn btn-primary ">Certificate Format From</a></p>
              <p><select class="form-select" aria-label="Default select example">
                 <option selected>--Select Certificate Format--</option>
              <option value="1">Principal Certified</option>
              <option value="2">District Panchayat Certified</option>
              <option value="3">Legislator Certified</option>
              <option value="3">MP Certified</option>
              <option value="3">Municipality Certified</option>
              <option value="3">Citycorporation Certified</option>
              </select></p>
              <div class="d-flex justify-content-between align-items-center">
               
              </div>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card shadow-sm">
           

            <div class="card-body">
              <p class="card-text"><a href="#" class="btn btn-primary ">Resum Format From</a></p>
              <p class="card-text"><a href="https://www.jobseeker.com/app/letters/f64a3ba8-c0c5-4378-b43a-d7348ea941a3/edit" target="e_blank">Making Reusme With All Format</a></p>

              <div class="d-flex justify-content-between align-items-center">
              </div>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card shadow-sm">
            

            <div class="card-body">
               <p class="card-text"><a href="#" class="btn btn-primary ">Official Online or Offline Form</a></p>
              <p><select class="form-select" aria-label="Default select example">
                 <option selected>--Select Online or Offline Form PDF--</option>
              <!-- <option value="1">Principal Certified</option>
              <option value="2">District Panchayat Certified</option>
              <option value="3">Legislator Certified</option>
              <option value="3">MP Certified</option>
              <option value="3">Municipality Certified</option>
              <option value="3">Citycorporation Certified</option> -->
              </select></p>
              <div class="d-flex justify-content-between align-items-center">
              </div>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card shadow-sm">
           

            <div class="card-body">
             <p class="card-text"><a href="#" class="btn btn-primary ">All PDF Converter</a></p>
              <p><select class="form-select" aria-label="Default select example">
                <option selected>--Select PDF Converter--</option>
              <option value="1">JPG to PDF</option>
              <option value="2">WORD to PDF</option>
              <option value="3">POWERPOINT to PDF</option>
              <option value="3">EXCEL to PDF</option>
              <option value="3"> HTML to PDF</option>
              <option value="3">EDIT PDF</option>
              </select></p>
              <div class="d-flex justify-content-between align-items-center">
              </div>
            </div>
          </div>
        </div>
<div class="col">
          <div class="card shadow-sm">
            

            <div class="card-body">
               <p class="card-text"><a href="#" class="btn btn-primary ">Remove Background </a></p>

               <p class="card-text"><a href="https://www.remove.bg/upload">Remove the Background</a></p>

              <div class="d-flex justify-content-between align-items-center">
              </div>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card shadow-sm">
            

            <div class="card-body">
               <p class="card-text"><a href="#" class="btn btn-primary ">PhotoSignatureResize</a></p>

               <p class="card-text"><a href="photosigresize.php">Resize Image Online</a></p>

              <div class="d-flex justify-content-between align-items-center">
              </div>
            </div>
          </div>
        </div>
        
         <div class="col">
          <div class="card shadow-sm">
           

            <div class="card-body">
             <p class="card-text"><a href="#" class="btn btn-primary ">All Design Format</a></p>
              <p><select class="form-select" aria-label="Default select example">
                <option selected>--Select Design Format--</option>
              <option value="1">All Id Card</option>
              <option value="2">Invitation Card</option>
              <option value="3">Poster Design</option>
              <option value="3">Receipts Design</option>
              <!-- <option value="3"> HTML to PDF</option>
              <option value="3">EDIT PDF</option> -->
              </select></p>
              <div class="d-flex justify-content-between align-items-center">
              </div>
            </div>
          </div>
        </div>
<div>
        
        
      </div>
    </div>
  </div>

  
</main>



<?php  include('footer.php'); ?>
<script src="js/bootstrap.bundle.min.js"></script>

    </body>
</html>
